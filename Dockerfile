FROM openjdk:8-jre-alpine
ADD build/libs/CouponApp-0.0.1-SNAPSHOT.jar CouponApp.jar
EXPOSE 31212
ENTRYPOINT ["java","-jar","/CouponApp.jar"]
